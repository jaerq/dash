import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import CityCard from './CityCard';

const styles = theme => ({
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'center',
      maxWidth: 1000,
      minWidth: 500,
      margin: 'auto'
    },
    contentWrapper: {
      margin: '40px 16px',
      flexGrow: 1,
      maxWidth: 1000
    }
  });
  

class Content extends React.Component {
   renderCards() {
    let cards = [];

    const airports = {
      "Atlanta": 'KATL',
      "Beijing": 'ZBAA',
      "Dubai": 'OMDB',
      "Los Angeles": 'KLAX',
      "Tokyo": 'RJTT',
      "Chicago": 'KORD',
      "London": 'EGLL',
      "Hong Kong": 'VHHH',
      "Shanghai": 'ZSPD',
      "France": 'LFPG'
    };

    for (let airport in airports) {
      cards.push(
        <Grid item xs={3} key={airport}>
          <CityCard name={airport} airports={airports[airport]}/>
        </Grid>
      )
    }
    
    return cards;
   };

   render() {
    return (
      <Paper className={this.props.classes.paper}>
        <div className={this.props.classes.contentWrapper}>
          <Grid container spacing={16} alignItems="center">
            {this.renderCards()}
          </Grid>
        </div>
      </Paper>
    );
}};

export default withStyles(styles)(Content);