import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
        fontWeight: theme.typography.fontWeightBold,
        textAlign: 'center',
        marginTop: 15
    },
    table: {
        maxHeight: 400,
        marginBottom: 100,
        marginTop: 5,
        overflowX: 'visible'
    }
});

let id = 0;

class ModalContent extends React.Component {
    _isMounted = false;

    state = {
        arrivingFlights: [],
        departingFlights: [],
        beginTime: 0
    };

    componentDidMount() {
        this._isMounted = true;
        this.fetchFlights('departure');
        this.fetchFlights();
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.beginTime !== this.state.beginTime) {
            this.fetchFlights('departure');
            this.fetchFlights();
        }
    };

    componentWillUnmount() {
        this._isMounted = false;
    };

    fetchFlights(type) {
        const API = 'https://opensky-network.org/api/flights/';
        const flightType = (type === 'departure') ? 'departure?' : 'arrival?';
        const airport = 'airport=' + this.props.icao;
        const currentTime = Math.round(new Date().getTime() / 1000);
        const begin = '&begin=' + (currentTime - this.state.beginTime);
        const end = '&end=' + currentTime;

        fetch(API + flightType + airport + begin + end)
            .then(response => response.json())
            .then(data => {
                if (this._isMounted) {
                    if (type === 'departure') {
                        this.setState({ departingFlights: data }, () => console.log(this.state.departingFlights))
                    } else {
                        this.setState({ arrivingFlights: data }, () => console.log(this.state.arrivingFlights))
                    }
                }
            });
    };

    handleChange = event => {
        let begin = event.target.value;
        this.setState({ beginTime: begin });
    };


    createData(icao, departing, arriving, callsign) {
        id += 1;
        return { id, icao, departing, arriving, callsign };
    };

    renderFlights(reqType) {
        const rows = [];
        let flights;

        if (reqType === 'departing') {
            flights = this.state.departingFlights
        } else {
            flights = this.state.arrivingFlights
        }

        for (let obj in flights) {
            rows.push(this.createData(
                flights[obj].icao24,
                flights[obj].estDepartureAirport,
                flights[obj].estArrivalAirport,
                flights[obj].callsign));
        }

        return (
            <Table className={this.props.classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>ICAO</TableCell>
                        <TableCell align="right">Departure Airport</TableCell>
                        <TableCell align="right">Arrival Airport</TableCell>
                        <TableCell align="right">Callsign</TableCell>
                    </TableRow>
                </TableHead>

                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.icao}
                            </TableCell>
                            <TableCell align="right">{row.departing}</TableCell>
                            <TableCell align="right">{row.arriving}</TableCell>
                            <TableCell align="right">{row.callsign}</TableCell>
                        </TableRow>
                    )
                    )}
                </TableBody>
            </Table>
        )
    }

    render() {
        return (
            <div className={this.props.classes.root}>
                <form autoComplete="off">
                    <FormControl>
                        <Select
                            value={this.state.beginTime}
                            onChange={this.handleChange}
                            inputProps={{
                                name: 'beginTime',
                                id: 'begin-simple',
                            }}
                        >
                            <MenuItem value={0}>
                                <em>Minutes</em>
                            </MenuItem>
                            <MenuItem value={18000}>300</MenuItem>
                            <MenuItem value={24000}>400</MenuItem>
                            <MenuItem value={27000}>450</MenuItem>
                            <MenuItem value={30000}>500</MenuItem>
                            <MenuItem value={57600}>960</MenuItem>
                            <MenuItem value={115200}>1920</MenuItem>
                        </Select>
                    </FormControl>
                </form>

                <Typography className={this.props.classes.heading}>Departure</Typography>
                {this.renderFlights('departing')}
                <Typography className={this.props.classes.heading}>Arrival</Typography>
                {this.renderFlights('arriving')}
            </div>
        )
    }
};



export default withStyles(styles)(ModalContent);