import React from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import ModalContent from './ModalContent';

const styles = theme => ({
  card: {
    minWidth: 100,
    '&:hover': {
      background: '#ebebeb'
    }},
  title: {
    fontSize: 14,
  },
  dialog: {
    maxWidth: 1000,
    overflowY: 'visible',
  }
});

class CityCard extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    })
  };

  handleClose = () => {
    this.setState({
      open: false
    })
  };

  render() {
    return (
      <React.Fragment>
        <Card className={this.props.classes.card} onClick={this.handleClickOpen}>
          <CardContent>
            <Typography className={this.props.classes.title} color="textSecondary" gutterBottom>
              {this.props.name}
            </Typography>
          </CardContent>
        </Card>

        <Dialog onClose={this.handleClose} open={this.state.open} >
          <DialogTitle onClose={this.handleClose}>
            {this.props.name}
          </DialogTitle>

          <DialogContent >
            <ModalContent time={this.state.beginTime} icao={this.props.airports} className={this.props.classes.dialog}/>
          </DialogContent>

          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>

      </React.Fragment>
    );
  }
}
    
export default withStyles(styles)(CityCard);