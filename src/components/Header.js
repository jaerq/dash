import React from 'react';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import auth from "../auth/auth.js";
import {withRouter} from 'react-router-dom'

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  }
});

class Header extends React.Component {


  render() {
    return (
      <React.Fragment>
        <AppBar
          component="div"
          className={this.props.classes.secondaryBar}
          color="primary"
          position="static"
          elevation={0}
        >
          <Toolbar>
            <Typography color="inherit" variant="h5">
              OpenSky
            </Typography>
          </Toolbar>
        </AppBar>

      </React.Fragment>
    );
  };
};


export default withRouter(withStyles(styles)(Header));