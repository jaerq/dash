class Auth{
    constructor() {
        this.authenticated = false;
    }

    login(username, password) {
        if (username === 'demo' && password === 'demo'){
        this.authenticated = true;
        }
    }

    logout() {
        this.authenticated = false;
    }
    isAuthenticated() {
        return this.authenticated;
    }
}

export default new Auth();