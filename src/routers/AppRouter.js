import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import SignIn from '../components/SignIn';
import Header from '../components/Header';
import Dashboard from '../components/Dashboard';
import {ProtectedRoute} from './ProtectedRoute';

const AppRouter = () => (
    <Router>
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={SignIn}/>
                <ProtectedRoute path="/dashboard" component={Dashboard} />
            </Switch>
        </div>
    </Router>
);

export default AppRouter;